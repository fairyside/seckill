package com.sifou.courses;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author liuzhongxu
 * @date 2020/7/4
 */
@EnableRetry
@EnableTransactionManagement
@EnableCaching
@SpringBootApplication
public class ApplicationStarter {

    public static void main(String[] args) {

        SpringApplication.run(ApplicationStarter.class);
    }
}
