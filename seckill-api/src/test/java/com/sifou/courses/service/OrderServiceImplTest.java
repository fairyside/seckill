package com.sifou.courses.service;

import com.sifou.courses.ApplicationStarter;
import com.sifou.courses.dto.SeckillGoodsDto;
import com.sifou.courses.dto.SeckillOrderDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author liuzhongxu
 * @date 2020/8/18
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApplicationStarter.class)
public class OrderServiceImplTest {

    @Resource
    private IOrderService orderService;

    @Test
    public void createOrder() {

        SeckillGoodsDto seckillGoodsDto = new SeckillGoodsDto();
        seckillGoodsDto.setId(1);
        seckillGoodsDto.setName("测试");
        orderService.createOrder(seckillGoodsDto);
    }

    @Test
    public void getOrders() {
        List<SeckillOrderDto> orderDtoList = orderService.getOrders(1, 10);
        System.out.println(orderDtoList);
    }
}