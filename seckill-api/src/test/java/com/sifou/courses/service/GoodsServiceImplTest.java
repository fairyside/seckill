package com.sifou.courses.service;

import com.sifou.courses.ApplicationStarter;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.redisson.api.RedissonClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.stream.IntStream;


/**
 * @author liuzhongxu
 * @date 2020/8/11
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApplicationStarter.class)
public class GoodsServiceImplTest {

    @Resource
    private IGoodsService goodsService;

    @Resource
    private RedissonClient redissonClient;

    @Before
    public void flushCache() {
//        redissonClient.getKeys().flushall();
//        log.debug("清空缓存");
    }

    @Test
    public void cocurrentGetGoodsById() {
        Integer id = 2;
        IntStream.range(0,10).parallel().forEach(i->{
            try {
                goodsService.getGoodsById(id);
            } catch (Exception e) {
                log.error(e.getMessage());
            }

        });
        try {
            Thread.sleep(1000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        IntStream.range(0,50).parallel().forEach(i->{
            try {
                goodsService.getGoodsById(id);
            } catch (Exception e) {
                log.error(e.getMessage());
            }

        });
        try {
            Thread.sleep(1000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        IntStream.range(0,100).parallel().forEach(i->{
            try {
                goodsService.getGoodsById(id);
            } catch (Exception e) {
                log.error(e.getMessage());
            }

        });

    }

    @Test
    public void getGoodsById() throws InterruptedException {
        Integer id = 1;
        for (int i = 0; i < 10; i++) {
            goodsService.getGoodsById(id);
            Thread.sleep(500L);
        }
    }

    @Test
    public void getGoodsNormal() {
        goodsService.getGoodsNormal(1, 10);
        goodsService.getGoodsNormal(1, 10);
        goodsService.getGoodsNormal(1, 10);
    }

    @Test
    public void getGoodsComplex() {
        goodsService.getGoodsComplex(1, 10);
        goodsService.getGoodsComplex(1, 10);
        goodsService.getGoodsComplex(1, 10);
        goodsService.getGoodsComplex(1, 10);
        goodsService.getGoodsComplex(1, 10);
    }

}