package com.sifou.courses.service;

import com.sifou.courses.ApplicationStarter;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author liuzhongxu
 * @date 2020/10/9
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApplicationStarter.class)
public class RateLimitTest {

    @Resource
    private IGoodsService goodsService;

    @Test
    public void testRateLimit() {
        AtomicInteger t= new AtomicInteger();
        for (int i = 0; i < 100; i++) {
            try {
                goodsService.getGoodsById2();
            } catch (Exception e) {
//                log.error(e.getMessage());
            }
            t.incrementAndGet();
            try {
                Thread.sleep(10L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
//        IntStream.range(0, 10000).parallel().forEach(i -> {
//            try {
//                goodsService.getGoodsById2();
//            } catch (Exception e) {
////                log.error(e.getMessage());
//            }
//            t.incrementAndGet();
//        });
        log.debug(String.valueOf(t.get()));
    }
}
