package com.sifou.courses.service;

import com.sifou.courses.ApplicationStarter;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.stream.IntStream;

/**
 * @author liuzhongxu
 * @date 2020/8/18
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApplicationStarter.class)
public class TradeServiceImplTest {

    @Resource
    private ITradeService tradeService;

    @Resource
    private IGoodsService goodsService;


    @Test
    public void doTrade() {
        int goodsId = 6;
        tradeService.doWrongTrade(goodsId);
    }

    @Test
    public void doWrongTrade() {
        int goodsId = 3;
        IntStream.range(0,100).parallel().forEach(i->{
            try {
                tradeService.doWrongTrade(goodsId);
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        });
    }

    @Test
    public void doOptimisticLockTrade() {
        int goodsId = 8;
        IntStream.range(0,100).parallel().forEach(i->{
            try {
                tradeService.doOptimisticLockTrade(goodsId);
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        });
    }

    @Test
    public void doTradeWithRedis() {
        int goodsId = 2;
        IntStream.range(0,100).parallel().forEach(i->{
            try {
                tradeService.doTradeWithRedis(goodsId);
            } catch (Exception e) {
                log.error(e.getMessage());
            }

        });
    }

    @Test
    public void doOptimisticLockTradeWithRedis() {
        int goodsId = 18;
        IntStream.range(0,100).parallel().forEach(i->{
            try {
                tradeService.doOptimisticLockTradeWithRedis(goodsId);
            } catch (Exception e) {
                log.error(e.getMessage());
            }

        });
    }

    @After
    public void after() throws InterruptedException {
        Thread.sleep(1000L);
    }
}