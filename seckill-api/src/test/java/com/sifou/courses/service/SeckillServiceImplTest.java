package com.sifou.courses.service;

import com.sifou.courses.ApplicationStarter;
import com.sifou.courses.dto.SeckillGoodsDto;
import com.sifou.courses.dto.SeckillOrderDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @author liuzhongxu
 * @date 2020/8/18
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApplicationStarter.class)
public class SeckillServiceImplTest {

    @Resource(name = "seckillService")
    private ISeckillService seckillService;

    @Resource
    private IOrderService orderService;

    @Test
    public void checkStock() {
        int goodsId = 1;
        seckillService.checkStock(goodsId);
    }

    @Test
    public void saleStock() {
        int goodsId = 6;
        // 检查库存
        SeckillGoodsDto goodsDto = seckillService.checkStock(goodsId);
        // 扣库存
        seckillService.saleStock(goodsDto);
    }

    @Test
    public void paySuccess() {
        int goodsId = 6;
        // 检查库存
        SeckillGoodsDto goodsDto = seckillService.checkStock(goodsId);
        // 扣库存
        seckillService.saleStock(goodsDto);
        // 下订单
        int orderId = orderService.createOrder(goodsDto);

        SeckillOrderDto orderDto = orderService.getOrder(orderId);
        // 修改订单状态
        orderService.updateOrderStatus(orderDto.getId(), 1);
    }

    @Test
    public void payFailure() {
        int goodsId = 6;
        // 检查库存
        SeckillGoodsDto goodsDto = seckillService.checkStock(goodsId);
        // 扣库存
        seckillService.saleStock(goodsDto);
        // 下订单
        int orderId = orderService.createOrder(goodsDto);

        SeckillOrderDto orderDto = orderService.getOrder(orderId);
        // 回滚库存
        seckillService.rollBackStock(orderDto);
        // 关闭订单
        orderService.updateOrderStatus(orderDto.getId(), 2);
    }
}