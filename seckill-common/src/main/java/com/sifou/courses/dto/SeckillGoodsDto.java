package com.sifou.courses.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 库存表
 * </p>
 *
 * @author liuzhongxu
 * @since 2020-08-11
 */
@Data
public class SeckillGoodsDto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Integer id;

    /**
     * 名称
     */
    private String name;

    /**
     * 限购
     */
    private Integer limit;

    /**
     * 库存
     */
    private Integer count;

    /**
     * 已售
     */
    private Integer sale;

    /**
     * 乐观锁，版本号
     */
    private Integer version;

    /**
     * 创建时间
     */
    @JSONField(format="yyyy-MM-dd")
    private Date createTime;

    /**
     * 更新时间
     */
    @JSONField(format="yyyy-MM-dd")
    private Date updateTime;


}
