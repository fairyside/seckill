package com.sifou.courses.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 库存订单表
 * </p>
 *
 * @author liuzhongxu
 * @since 2020-08-11
 */
@Data
public class SeckillOrderDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    /**
     * 商品ID
     */
    private Integer goodsId;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 订单状态 0 未支付 1 已支付 2 关闭
     */
    private Integer status;

    /**
     * 创建时间
     */
    @JSONField(format="yyyy-MM-dd")
    private Date createTime;


}
