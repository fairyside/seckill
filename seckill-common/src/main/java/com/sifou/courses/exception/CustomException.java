package com.sifou.courses.exception;

/**
 * @author liuzhongxu
 * @date 2020/8/18
 */
public class CustomException extends RuntimeException {

    public CustomException() {
        super();
    }

    public CustomException(String msg) {
        super(msg);
    }
}
