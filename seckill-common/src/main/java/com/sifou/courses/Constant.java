package com.sifou.courses;

/**
 * @author liuzhongxu
 * @date 2020/8/19
 */
public interface Constant {
    /**
     * redis-key-前缀-count-库存
     */
    String PREFIX_COUNT = "goods_count_";

    /**
     * redis-key-前缀-sale-已售
     */
    String PREFIX_SALE = "goods_sale_";

    /**
     * redis-key-前缀-version-乐观锁版本
     */
    String PREFIX_VERSION = "goods_version_";
}
