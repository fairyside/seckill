package com.sifou.courses.cache;

import lombok.Data;

/**
 * @author liuzhongxu
 * @date 2020/9/4
 */
@Data
public class LocalCache {

    private Object key;
    private Object value;
    private volatile Boolean needReload;

}
