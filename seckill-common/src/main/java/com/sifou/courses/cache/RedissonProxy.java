package com.sifou.courses.cache;

import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.RedissonBucket;
import org.redisson.RedissonBuckets;
import org.redisson.RedissonList;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.redisson.command.CommandAsyncExecutor;
import org.springframework.cache.Cache;
import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.InvocationHandler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.util.ObjectUtils;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

/**
 *
 * Cglib动态代理调用有参构造创建代理对象
 * @author liuzhongxu
 * @date 2020/9/2
 */
@Slf4j
public class RedissonProxy implements InvocationHandler {

    private RedissonClient redissonClient;

    private ThreadPoolTaskExecutor poolTaskExecutor;

    private Cache localCache;

    private Object target;

    private String name;

    public void setLocalCache(Cache localCache) {
        this.localCache = localCache;
    }

    public void setPoolTaskExecutor(ThreadPoolTaskExecutor poolTaskExecutor) {
        this.poolTaskExecutor = poolTaskExecutor;
    }

    public Object bind(Object target, Redisson redisson) {
        if (ObjectUtils.isEmpty(this.localCache)) {
            return target;
        }
        this.target = target;
        this.redissonClient = redisson;
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(this.target.getClass());
        // 回调方法
        enhancer.setCallback(this);
        if (RedissonBucket.class.equals(this.target.getClass())) {
            RedissonBucket redissonBucket = (RedissonBucket) this.target;
            this.name = redissonBucket.getName();
            // 创建代理对象
            return enhancer.create(new Class[]{CommandAsyncExecutor.class, String.class}, new Object[]{redisson.getCommandExecutor(), this.name});
        }
        if (RedissonBuckets.class.equals(this.target.getClass())) {
            // 创建代理对象
            return enhancer.create(new Class[]{CommandAsyncExecutor.class}, new Object[]{redisson.getCommandExecutor()});
        }
        if (RedissonList.class.equals(this.target.getClass())) {
            RedissonList list = (RedissonList) this.target;
            this.name = list.getName();
            return enhancer.create(new Class[]{CommandAsyncExecutor.class, String.class, RedissonClient.class}, new Object[]{redisson.getCommandExecutor(), this.name, redisson});
        }
        return this.target;
    }

    @Override
    public Object invoke(Object o, Method method, Object[] objects) throws Throwable {
        // 未开启本地缓存
        if (ObjectUtils.isEmpty(this.localCache)) {
            return method.invoke(target, objects);
        }

        if (method.getName().equalsIgnoreCase("get")) {

            // get key
            if (ObjectUtils.isEmpty(this.name)) {
                this.name = objects.toString();
            }

            // wrapper is null
            if (ObjectUtils.isEmpty(this.localCache.get(this.name))) {
                return method.invoke(target, objects);
            }

            // 本地缓存对象  会触发 load reload  if value=null load if REFRESH_TIME reload
            LocalCache localCacheBean = (LocalCache) this.localCache.get(this.name).get();

            // 本地缓存为空 同步加载
            if (ObjectUtils.isEmpty(localCacheBean.getValue())) {
                return invokeRedissonGet(localCacheBean, method, objects);
            }

            // 刷新本地缓存
            if (localCacheBean.getNeedReload()) {
                // double check sync
                synchronized (localCacheBean) {
                    if (localCacheBean.getNeedReload()) {
                        localCacheBean.setNeedReload(false);
                        // reload from redis to local
                        return invokeRedissonGet(localCacheBean, method, objects);
                    }
                }
            }

            return localCacheBean.getValue();
        }

        if (method.getName().equalsIgnoreCase("set")) {
            // set 永不过期，手动控制过期时间
            RedissonBucket redissonBucket = (RedissonBucket) this.target;
            SecondCache secondCache = new SecondCache();
            secondCache.setKey(this.name);
            secondCache.setValue(objects[0]);
            if (objects.length > 1) {
                long timeToLive = (long) objects[1];
                TimeUnit timeUnit = (TimeUnit) objects[2];
                secondCache.setTtl(timeUnit.toMillis(timeToLive));
            }
            redissonBucket.set(secondCache);
            log.debug("更新redis缓存 key={},value={}", this.name, secondCache);

            // write local cache
            LocalCache localCacheBean = new LocalCache();
            localCacheBean.setKey(this.name);
            localCacheBean.setValue(objects[0]);
            localCacheBean.setNeedReload(false);
            this.localCache.put(this.name, localCacheBean);
            log.debug("更新本地缓存 key={},value={}", this.name, localCacheBean);
            return null;
        }

        return method.invoke(target, objects);
    }

    Object invokeRedissonGet(LocalCache localCacheBean, Method method, Object[] objects) throws Throwable {
        // 执行redis get
        SecondCache secondCache = (SecondCache) method.invoke(target, objects);

        // 首次访问
        if (ObjectUtils.isEmpty(secondCache) && ObjectUtils.isEmpty(localCacheBean.getValue())) {
            log.debug("first load. localCacheBean=null,secondCache=null");
            return null;
        }

        // secondCache is not empty , load from redis to local
        if (!ObjectUtils.isEmpty(secondCache)) {
            log.debug("load from redis to local. secondCache={}", secondCache);
            localCacheBean.setValue(secondCache.getValue());
        }

        // if secondCache needReload
        if (ObjectUtils.isEmpty(secondCache) || secondCache.getNeedReload()) {
            //  set nx 1  放1线程，其他返回一级缓存，
            RBucket<Object> lock = redissonClient.getBucket(localCacheBean.getKey().toString() + "_lock");
            if (lock.trySet(1, 2L, TimeUnit.SECONDS)) {
                log.debug("reload secondCache key={}", localCacheBean.getKey());
                return null;
            }
        }

        log.debug("return localCache={}", localCacheBean.getValue());
        // 返回一级缓存
        return localCacheBean.getValue();
    }

    @Deprecated
    void invokeRedissonGetAsync(LocalCache localCacheBean, Method method, Object[] objects) {
        this.poolTaskExecutor.execute(() -> {
            try {
                log.debug("AsyncGetResult start. ");
                invokeRedissonGet(localCacheBean, method, objects);
                log.debug("AsyncGetResult end. ");
            } catch (Throwable throwable) {
                log.error(throwable.getMessage());
            }
        });
    }

}
