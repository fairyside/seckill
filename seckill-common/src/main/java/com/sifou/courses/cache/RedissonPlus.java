package com.sifou.courses.cache;

import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.RedissonBucket;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author liuzhongxu
 * @date 2020/9/1
 */
@Slf4j
@Component
public class RedissonPlus {

    private static final String DEFAULT_CACHE = "default";

    @Resource
    private CacheManager caffeine;

    @Resource
    private RedissonClient redissonClient;

    @Value("${seckill.localcache}")
    private boolean initLocalCache;

    @Autowired
    private ThreadPoolTaskExecutor poolTaskExecutor;

    private RedissonProxy createProxy() {
        RedissonProxy redissonProxy = new RedissonProxy();
        if (!initLocalCache) {
            return redissonProxy;
        }
        redissonProxy.setLocalCache(caffeine.getCache(DEFAULT_CACHE));
        poolTaskExecutor.setThreadNamePrefix("RedissonPlus-");
        poolTaskExecutor.setWaitForTasksToCompleteOnShutdown(true);
        redissonProxy.setPoolTaskExecutor(poolTaskExecutor);
        return redissonProxy;
    }

    public <V> RBucket<V> getBucket(String name) {
        RBucket<V> bucket = redissonClient.getBucket(name);
        RedissonBucket bucket1 = (RedissonBucket) createProxy().bind(bucket, (Redisson) redissonClient);
        return (RBucket<V>) bucket1;
    }
}
