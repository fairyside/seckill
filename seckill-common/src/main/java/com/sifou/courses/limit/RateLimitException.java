package com.sifou.courses.limit;

/**
 * @author liuzhongxu
 * @date 2020/10/9
 */
public class RateLimitException extends RuntimeException {

    private static final long serialVersionUID = 7546207978436145956L;

    public RateLimitException() {
    }

    public RateLimitException(String message) {
        super(message);
    }

    public RateLimitException(String message, Throwable cause) {
        super(message, cause);
    }

    public RateLimitException(Throwable cause) {
        super(cause);
    }

    public RateLimitException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
