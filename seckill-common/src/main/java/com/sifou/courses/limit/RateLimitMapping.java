package com.sifou.courses.limit;

import com.google.common.util.concurrent.RateLimiter;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @author liuzhongxu
 * @date 2020/10/10
 */
@Data
@Component
class RateLimitMapping {

    private ConcurrentHashMap<String, RateLimiter> singleLimiterMap = new ConcurrentHashMap<>();

}
