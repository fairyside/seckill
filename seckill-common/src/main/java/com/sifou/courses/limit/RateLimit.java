package com.sifou.courses.limit;

import java.lang.annotation.*;

/**
 * @author liuzhongxu
 * @date 2020/9/24
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface RateLimit {

    int limitNum() default 10;

    LimitType limitType() default LimitType.single;

    enum LimitType{
        single,cluster
    }
}
