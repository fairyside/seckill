# seckill

#### 介绍
秒杀系统实战

## 项目搭建

### 项目技术：

#### 后端技术选型为：
jdk1.8、SpringBoot 2.3.1.RELEASE 、MyBatis、MyBatis-Plus 、Redis、Elasticsearch、Logback 、Rocketmq-4.7.1、Swagger2、Thymeleaf等。

#### 开发工具：
IntelliJ IDEA x64、MySQL 5.7、Mac、Lombok、Maven、Git、Navicat、Postman

#### 模块依赖关系
![输入图片说明](https://images.gitee.com/uploads/images/2020/0821/004728_e6f4b2e0_1128635.png "工程结构.png")

## 存储设计
### 数据库设计（Mysql）
#### 用户
- 商品表 t_seckill_goods
- 订单表 t_seckill_order

#### MyBatis-Plus Generator
[官网-代码生成器](https://mybatis.plus/guide/generator.html)

### 缓存设计（Redis）



## 业务设计


