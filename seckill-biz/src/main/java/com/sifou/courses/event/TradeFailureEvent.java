package com.sifou.courses.event;

import com.sifou.courses.service.ISeckillService;
import lombok.Data;

/**
 * @author liuzhongxu
 * @date 2020/8/21
 */
@Data
public class TradeFailureEvent {

    private Integer orderId;
    private ISeckillService seckillService;
}
