package com.sifou.courses.service;

import com.sifou.courses.dto.SeckillOrderDto;
import com.sifou.courses.event.TradeFailureEvent;
import org.springframework.retry.RetryException;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * 交易回调接口
 * @author liuzhongxu
 * @date 2020/8/21
 */
public interface ITradeCallBackService {
    /**
     * 支付成功队列
     */
    BlockingQueue<Integer> paySuccess = new LinkedBlockingQueue<>();

    /**
     * 支付失败队列
     */
    BlockingQueue<TradeFailureEvent> payFailure = new LinkedBlockingQueue();

    /**
     * 交易成功
     * @param orderDto
     * @return
     */
    Integer doCallBackSuccess(SeckillOrderDto orderDto);

    /**
     * 交易失败
     *
     * @param orderDto
     * @return
     */
    Integer doCallBackFailure(ISeckillService seckillService, SeckillOrderDto orderDto);

    /**
     * 交易重试失败，最后一次回调
     * @param e
     * @return
     */
    Integer doCallBackRecover(RetryException e);
}
