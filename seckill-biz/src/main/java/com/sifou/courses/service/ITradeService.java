package com.sifou.courses.service;

/**
 * @author liuzhongxu
 * @date 2020/8/18
 */
public interface ITradeService {

    Integer doWrongTrade(Integer id);

    Integer doOptimisticLockTrade(Integer id);

    Integer doOptimisticLockTradeWithRedis(Integer id);

    Integer doTradeWithRedis(Integer id);

}
