package com.sifou.courses.service;

import com.sifou.courses.dto.SeckillGoodsDto;

import java.util.List;

/**
 * @author liuzhongxu
 * @date 2020/8/11
 */
public interface IGoodsService {

    /**
     * 缓存预热
     */
    void initCache();

    /**
     * 查询商品信息
     * @param id
     * @return
     */
    SeckillGoodsDto getGoodsById(Integer id);

    /**
     * 批量查询商品信息
     * @param ids
     * @return
     */
    List<SeckillGoodsDto> getGoodsByIds(List<Integer> ids);

    /**
     * 分页查询商品信息，按分页整体缓存商品信息
     * @param page
     * @param size
     * @return
     */
    List<SeckillGoodsDto> getGoodsNormal(int page, int size);

    /**
     * 分页查询商品信息，缓存全量商品id，应用内分页，最后组装商品信息
     * @param page
     * @param size
     * @return
     */
    List<SeckillGoodsDto> getGoodsComplex(int page, int size);

    SeckillGoodsDto getGoodsById2() throws InterruptedException;
}
