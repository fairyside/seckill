package com.sifou.courses.service;

import com.sifou.courses.dto.SeckillGoodsDto;
import com.sifou.courses.dto.SeckillOrderDto;

/**
 * @author liuzhongxu
 * @date 2020/8/18
 */
public interface ISeckillService {

    /**
     * 检查库存
     * @param id 商品id
     * @return
     */
    SeckillGoodsDto checkStock(Integer id);

    /**
     * 扣库存
     * @param goodsDto
     * @return
     */
    Integer saleStock(SeckillGoodsDto goodsDto);

    /**
     * 回滚库存
     * @param orderDto
     * @return
     */
    Integer rollBackStock(SeckillOrderDto orderDto);
}
