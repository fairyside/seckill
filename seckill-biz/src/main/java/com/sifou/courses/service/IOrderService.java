package com.sifou.courses.service;

import com.sifou.courses.dto.SeckillGoodsDto;
import com.sifou.courses.dto.SeckillOrderDto;

import java.util.List;

/**
 * @author liuzhongxu
 * @date 2020/8/17
 */
public interface IOrderService {

    /**
     * 查询订单
     * @param orderId
     * @return
     */
    SeckillOrderDto getOrder(Integer orderId);

    /**
     * 下订单
     * @param goodsDto
     * @return
     */
    Integer createOrder(SeckillGoodsDto goodsDto);

    /**
     * 查询订单列表
     * @param page
     * @param size
     * @return
     */
    List<SeckillOrderDto> getOrders(int page, int size);

    /**
     * 更新订单状态
     * @param orderId
     * @param status
     * @return
     */
    Integer updateOrderStatus(int orderId, int status);
}
