package com.sifou.courses.repository.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 订单表
 * </p>
 *
 * @author liuzhongxu
 * @since 2020-08-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TSeckillOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 商品ID
     */
    private Integer goodsId;

    /**
     * 商品名称
     */
    @TableField("`name`")
    private String name;

    /**
     * 订单状态 0 未支付 1 已支付 2 关闭
     */
    @TableField("`status`")
    private Integer status;

    /**
     * 创建时间
     */
    private Date createTime;


}
