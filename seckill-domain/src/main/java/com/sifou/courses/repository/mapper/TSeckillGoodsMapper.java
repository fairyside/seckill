package com.sifou.courses.repository.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sifou.courses.repository.entity.TSeckillGoods;
import org.apache.ibatis.annotations.Update;

/**
 * <p>
 * 商品表 Mapper 接口
 * </p>
 *
 * @author liuzhongxu
 * @since 2020-08-17
 */
public interface TSeckillGoodsMapper extends BaseMapper<TSeckillGoods> {

    /**
     * 乐观锁更新扣减库存
     * @param tSeckillGoods
     * @return
     */
    @Update("UPDATE t_seckill_goods SET count = count - 1, sale = sale + 1, version = version + 1 " +
            "WHERE id = #{id, jdbcType = INTEGER} AND count > 0 AND version = #{version, jdbcType = INTEGER} ")
    int updateByOptimisticLock(TSeckillGoods tSeckillGoods);

    /**
     * 乐观锁回滚库存
     * @param tSeckillGoods
     * @return
     */
    @Update("UPDATE t_seckill_goods SET count = count + 1, sale = sale - 1, version = version + 1 " +
            "WHERE id = #{id, jdbcType = INTEGER} AND version = #{version, jdbcType = INTEGER} ")
    int rollBackByOptimisticLock(TSeckillGoods tSeckillGoods);

    /**
     * 更新扣减库存
     * @param tSeckillGoods
     * @return
     */
    @Update("UPDATE t_seckill_goods SET count = count - 1, sale = sale + 1 " +
            "WHERE id = #{id, jdbcType = INTEGER} AND count > 0")
    int updateStock(TSeckillGoods tSeckillGoods);

    /**
     * 回滚库存
     * @param tSeckillGoods
     * @return
     */
    @Update("UPDATE t_seckill_goods SET count = count + 1, sale = sale - 1 " +
            "WHERE id = #{id, jdbcType = INTEGER}")
    int rollBackStock(TSeckillGoods tSeckillGoods);
}
