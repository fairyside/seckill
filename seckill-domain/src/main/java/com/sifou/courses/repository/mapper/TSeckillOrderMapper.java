package com.sifou.courses.repository.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sifou.courses.repository.entity.TSeckillOrder;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author liuzhongxu
 * @since 2020-08-17
 */
public interface TSeckillOrderMapper extends BaseMapper<TSeckillOrder> {

}
