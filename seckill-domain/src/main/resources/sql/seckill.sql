-- 删除数据库
drop database seckill;
-- 创建数据库
create database seckill;
-- 使用数据库
use seckill;
-- 创建商品表
DROP TABLE IF EXISTS `t_seckill_goods`;
CREATE TABLE `t_seckill_goods` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '商品ID',
  `name` varchar(50) NOT NULL DEFAULT 'iPhone 11 Pro' COMMENT '名称',
  `limit` int(11) NOT NULL DEFAULT 1 COMMENT '限购',
  `count` int(11) NOT NULL COMMENT '库存',
  `sale` int(11) NOT NULL COMMENT '已售',
  `version` int(11) NOT NULL COMMENT '乐观锁，版本号',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='商品表';
-- 插入20个商品，初始化10个库存
INSERT INTO `t_seckill_goods` (`count`, `sale`, `version`) VALUES ('10', '0', '0');
INSERT INTO `t_seckill_goods` (`count`, `sale`, `version`) VALUES ('10', '0', '0');
INSERT INTO `t_seckill_goods` (`count`, `sale`, `version`) VALUES ('10', '0', '0');
INSERT INTO `t_seckill_goods` (`count`, `sale`, `version`) VALUES ('10', '0', '0');
INSERT INTO `t_seckill_goods` (`count`, `sale`, `version`) VALUES ('10', '0', '0');
INSERT INTO `t_seckill_goods` (`count`, `sale`, `version`) VALUES ('10', '0', '0');
INSERT INTO `t_seckill_goods` (`count`, `sale`, `version`) VALUES ('10', '0', '0');
INSERT INTO `t_seckill_goods` (`count`, `sale`, `version`) VALUES ('10', '0', '0');
INSERT INTO `t_seckill_goods` (`count`, `sale`, `version`) VALUES ('10', '0', '0');
INSERT INTO `t_seckill_goods` (`count`, `sale`, `version`) VALUES ('10', '0', '0');
INSERT INTO `t_seckill_goods` (`count`, `sale`, `version`) VALUES ('10', '0', '0');
INSERT INTO `t_seckill_goods` (`count`, `sale`, `version`) VALUES ('10', '0', '0');
INSERT INTO `t_seckill_goods` (`count`, `sale`, `version`) VALUES ('10', '0', '0');
INSERT INTO `t_seckill_goods` (`count`, `sale`, `version`) VALUES ('10', '0', '0');
INSERT INTO `t_seckill_goods` (`count`, `sale`, `version`) VALUES ('10', '0', '0');
INSERT INTO `t_seckill_goods` (`count`, `sale`, `version`) VALUES ('10', '0', '0');
INSERT INTO `t_seckill_goods` (`count`, `sale`, `version`) VALUES ('10', '0', '0');
INSERT INTO `t_seckill_goods` (`count`, `sale`, `version`) VALUES ('10', '0', '0');
INSERT INTO `t_seckill_goods` (`count`, `sale`, `version`) VALUES ('10', '0', '0');
INSERT INTO `t_seckill_goods` (`count`, `sale`, `version`) VALUES ('10', '0', '0');

-- 创建订单表
DROP TABLE IF EXISTS `t_seckill_order`;
CREATE TABLE `t_seckill_order` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `name` varchar(30) NOT NULL DEFAULT 'iPhone 11 Pro' COMMENT '商品名称',
  `status` int(11) not null default 0 comment '订单状态 0 未支付 1 已支付 2 关闭',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='订单表';
